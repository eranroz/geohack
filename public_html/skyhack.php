<?PHP
ob_start("ob_gzhandler");
#error_reporting( E_ALL );
#ini_set("display_errors",1);

/**
 skyhack by user:kolossos@wikipedia  Released under GPL
based on geohack (c)  by
Magnus Manske (2006)
 Released under GPL
 geo_param.php is (c) 2005, Egil Kvaleberg <egil@kvaleberg.no>, also GPL
*/

#include "../common.php" ;
#ini_set('user_agent','MSIE 4\.0b2;'); # Fake user agent
ini_set('user_agent','skyhack'); # Fake user agent

#include "geo_param.php" ;
#include "mapsources.php" ;

$name=addslashes($_GET[name]);
$object=addslashes($_GET[object]);
if ($object=="") {$object=$name; }

$name_url=str_replace(" ","",$name);
$ra=addslashes($_GET[ra]);            #Rektaszension hh mm ss
$de=addslashes($_GET[de]);            #Deklination    +/-ddd mm ss

$lon=15*$ra - 180;
$lat=$de;

$size=addslashes($_GET[size]);       #App-dim in degrees   (beta)
if ( $size==''){$size=0.01;}

$size_minute=$size*60;
$beta=0.0087266*$size;
$range = 6378000 *(1.1917536 * sin($beta) - cos($beta) + 1); 
$z=floor(-log($size/90,2));

if ($size_minute < 10)
{   $radius = 12;}
else {
if ($size_minute > 50){$radius = 60;} else {$radius= 1.2 * $size_minute;}
}



function fix_language_code ( $lang , $default = "en" ) {
  $lang = trim ( strtolower ( $lang ) ) ;
  if ( preg_match ( "/^([\-a-z]+)/" , $lang , $l ) ) {
    $lang = $l[0] ;
  } else $lang = $default ; // Fallback
  return $lang ;
}

function get_request ( $key , $default = "" ) {
    global $prefilled_requests ;
    if ( isset ( $prefilled_requests[$key] ) ) return $prefilled_requests[$key] ;
    if ( isset ( $_REQUEST[$key] ) ) return str_replace ( "\'" , "'" , $_REQUEST[$key] ) ;
    return $default ;
}



set_time_limit ( 20 ) ; # 20 sec should be enough for everyone!

$lang = fix_language_code ( get_request ( 'language' , 'en' ) , '' ) ;
if ( $lang == '' ) {
    print "No language given!" ;
    exit ;
}

$test = isset ( $_REQUEST['test'] ) ;


# Read template
$pagename = "Template:SkyhackTool" ;
#$pagename = "User:Magnus_Manske/skyhack" ;
#$pagename = get_request ( "altpage" , "Template:GeoTemplate" ) ;
$page = @file_get_contents ( "http://meta.wikimedia.org/wiki/$pagename?action=render" ) ;

if ( false === $page || '' == $page ) {
    print "Failed to open template. Please try again in a moment." ;
    exit ;
}

$page = str_replace ( ' href="/w' , " href=\"http://{$lang}.wikipedia.org/w" , $page ) ;

# Separate content
#$page = array_pop ( explode ( '<!-- start skyhack -->' , $page , 2 ) ) ;
#$page = array_shift ( explode ( '<!-- end skyhack -->' , $page , 2 ) ) ;

# Remove edit links
do {
    $op = $page ;
    $p = explode ( '<span class="editsection"' , $page , 2 ) ;
    if ( count ( $p ) == 1 ) continue ;
    $page = array_shift ( $p ) ;
    $p = explode ( '</span>' , array_pop ( $p ) , 2 ) ;
    $page .= array_pop ( $p ) ;
} while ( $op != $page ) ;

# Replace text
#$md = new map_sources ( $theparams , "Some title" ) ;

for ( $a = 1 ; $a <= 5 ; $a++ ) {
	$k = 'viz' . $a ;
	$k2 = '{' . $k . '}' ;
	$k3 = '{' . $k . 'text}' ;
	if ( isset ( $_REQUEST[$k] ) ) {
		$page = str_replace ( $k2 , urlencode ( $_REQUEST[$k] ) , $page ) ;
		$page = str_replace ( $k3 , $_REQUEST[$k] , $page ) ;
	} else {
		$p = explode ( $k2 , $page , 2 ) ;
		if ( count ( $p ) != 2 ) continue ;
		$p1 = explode ( '<li>' , $p[0] ) ;
		$p2 = explode ( '</li>' , $p[1]) ;
		array_pop ( $p1 ) ;
		array_shift ( $p2 ) ;
		$page = implode ( '<li>' , $p1 ) ;
		$page .= implode ( '</li>' , $p2 ) ;
		$page = str_replace ( $k2 , urlencode ( $_REQUEST[$k] ) , $page ) ;
	}
}


$page = str_replace ( '{ra}' ,$ra, $page ) ;
$page = str_replace ( '{de}' ,$de, $page ) ;
$page = str_replace ( '{latdegdec}' ,$lat, $page ) ;
$page = str_replace ( '{londegdec}' ,$lon, $page ) ;
$page = str_replace ( '{name}' ,$name, $page ) ;
$page = str_replace ( '{name_url}' ,$name_url, $page ) ;
$page = str_replace ( '{title}' ,$name, $page ) ;
$page = str_replace ( '{object}' ,$object, $page ) ;

$page = str_replace ( '{size}' ,$size, $page ) ;
$page = str_replace ( '{size_minute}' ,$size_minute, $page ) ;
$page = str_replace ( '{z}' ,$z, $page ) ;
$page = str_replace ( '{range}' ,$range, $page ) ;
$page = str_replace ( '{radius}' ,$radius, $page ) ;


# Output
print "<html><head><title>Mapsources for the sky</title>" ;
print "<meta http-equiv='content-type' content='text/html; charset=utf-8'>" ;
print '<link rel="stylesheet" type="text/css" media="screen" href="http://en.wikipedia.org/skins-1.5/monobook/main.css" >' ;
print $stylesheets ;
print '</head><body style="margin-left:5px; margin-right:5px;">' ;
#print get_common_header ( "geohack.php" ) ;
print '

<div style="float:right">
 <a href="http://tools.wmflabs.org">
 <img style="vertical-align:top"  height="40px" border="0" alt="Powered by WMF Labs" src="//wikitech.wikimedia.org/w/images/c/cf/Labslogo_thumb.png" />
 </a>
</div>

<span style="left:5px;top:2px;position:relative;border:2px solid red;display:inline;float:left;padding:2px;font-size:150%;background-color:white"><a target="_blank" href="http://wikimediafoundation.org/wiki/Fundraising">Donate to Wikimedia!</a></span>

<center style="width:100%; border-bottom:1px solid #AAAAAA;margin-bottom:3px;padding:2px;background-color:#AAFFAA">
This is based on the <i>mapsources</i> extension by <a href="http://en.wikipedia.org/wiki/User:Egil">en:User:Egil</a>, extended by <a href="http://en.wikipedia.org/wiki/User:Magnus_Manske">Magnus Manske</a> and <a href="http://de.wikipedia.org/wiki/Benutzer:Kolossos">User:Kolossos</a>.
<br/>
For the source of this script, see
<a href="./skyhack-source.php">skyhack-source.php</a>.
</center>
' ;

print '
<div id="globalWrapper">
<div>

<div style="float:left">
<img src="http://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Orion_Nebula_-_Hubble_2006_mosaic.jpg/140px-Orion_Nebula_-_Hubble_2006_mosaic.jpg" />
</div>

<div id="content" style="margin-top:0px;margin-left:145px">
<div id="bodyContent">
<h1>Map sources/SkyHack</h1>
' ;

print $page ;

print '
</div>
</div>
</div>
</div>
</body>
</html>
' ;

?> 
